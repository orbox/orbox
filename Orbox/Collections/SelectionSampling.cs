using System.Collections.Generic;
using UnityEngine;

namespace Orbox.Collections
{
    public static class SelectionSampling
    {
        //---------   Select collection N random elements from collection   ---------
        public static void SelectRandom<T>(this List<T> source, int count, List<T> toFill)
        {
            // Selection sampling algorithm is using

            var maxCount = source.Count;

            foreach (var element in source)
            {
                var probability = (float)count / maxCount;
                var random = UnityEngine.Random.Range(0f, 1f);

                if (random <= probability)
                {
                    count--;
                    toFill.Add(element);
                }

                maxCount--;

                if (count == 0)
                {
                    break;
                }
            }

        }
    }
}
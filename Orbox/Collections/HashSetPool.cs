using System;
using System.Collections.Generic;

namespace Orbox.Collections
{

    public class HashSetPool<T>
    {
        private Stack<PoolableHashSet<T>> Pool = new Stack<PoolableHashSet<T>>();

        public PoolableHashSet<T> Rent()
        {
            PoolableHashSet<T> item;

            if (Pool.Count > 0)
            {
                item = Pool.Pop();
            }
            else
            {
                item = new PoolableHashSet<T>(this);
            }

            return item;
        }

        public void Return(PoolableHashSet<T> item)
        {
            item.Clear();
            Pool.Push(item);
        }
    }

    public class PoolableHashSet<T> : HashSet<T>, IDisposable
    {
        private HashSetPool<T> Pool;

        public PoolableHashSet(HashSetPool<T> pool)
        {
            Pool = pool;
        }

        public void Dispose()
        {
            Pool.Return(this);
        }
    }



}
using System;
using System.Collections.Generic;

namespace Orbox.Collections
{
    
    public class DictionaryPool<TKey, TValue>
    {
        private Stack<PoolableDictionary<TKey,TValue>> Pool = new Stack<PoolableDictionary<TKey, TValue>>();

        public PoolableDictionary<TKey, TValue> Rent()
        {
            PoolableDictionary<TKey, TValue> item;

            if (Pool.Count > 0)
            {
                item = Pool.Pop();
            }
            else
            {
                item = new PoolableDictionary<TKey, TValue>(this);
            }

            return item;
        }

        public void Return(PoolableDictionary<TKey, TValue> item)
        {
            item.Clear();
            Pool.Push(item);
        }
    }

    public class PoolableDictionary<TKey, TValue> : Dictionary<TKey, TValue>, IDisposable
    {
        private DictionaryPool<TKey, TValue> Pool;

        public PoolableDictionary(DictionaryPool<TKey, TValue> pool)
        {
            Pool = pool;
        }

        public void Dispose()
        {
            Pool.Return(this);
        }
    }

    

}